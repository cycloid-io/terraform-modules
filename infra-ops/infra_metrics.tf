## EIP

resource "aws_eip" "metrics" {
  instance = "${element(aws_instance.metrics.*.id, count.index)}"
  vpc      = true
}

## SECURITY GROUP
# We currently use graphite on metrics
resource "aws_security_group" "metrics" {
  name        = "metrics"
  description = "Allow graphite web"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${split(",", var.metrics_allowed_networks)}"]
  }

  ingress {
    from_port   = 2003
    to_port     = 2003
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.monitoring.private_ip}/32"]
  }

  tags {
    Name     = "metrics"
    client   = "${var.customer}"
    project  = "infra"
  }
}

## INSTANCES
resource "aws_instance" "metrics" {
  ami                  = "${lookup(var.debian_jessie_ami, var.aws_region)}"
  count                = 1
  iam_instance_profile = "${var.iam_instance_profile}"
  instance_type        = "${var.metrics_instance_type}"
  key_name             = "${var.keypair_name}"
  vpc_security_group_ids      = [
      "${aws_security_group.metrics.id}",
      "${aws_security_group.allow_bastion.id}"
  ]
  subnet_id            = "${element(split(",", var.public_subnets_ids), count.index)}"

  tags {
    Name     = "${upper(var.customer)}-METRICS${count.index}-${upper(lookup(var.short_region, var.aws_region))}-INFRA"
    client   = "${var.customer}"
    env      = "infra"
    project  = "infra"
    role     = "metrics"
  }
}

## EBS
resource "aws_ebs_volume" "metrics" {
  availability_zone = "${aws_instance.metrics.availability_zone}"
  size              = "${var.metrics_ebs_size}"
  type              = "gp2"

  tags {
    Name     = "METRICS${count.index}--${upper(lookup(var.short_region, var.aws_region))}-INFRA-EBS"
    client   = "${var.customer}"
    env      = "infra"
    project  = "infra"
    role     = "metrics"
  }
}

resource "aws_volume_attachment" "metrics_attachment" {
  device_name = "/dev/xvdf"
  instance_id = "${aws_instance.metrics.id}"
  volume_id   = "${aws_ebs_volume.metrics.id}"
}

## CLOUDWATCH ALARMS
resource "aws_cloudwatch_metric_alarm" "recover-metrics" {
  alarm_actions             = ["arn:aws:automate:${var.aws_region}:ec2:recover"]
  alarm_description         = "Recover the instance"
  alarm_name                = "recover-graph"
  comparison_operator       = "GreaterThanThreshold"
  dimensions                = {
    InstanceId = "${element(aws_instance.metrics.*.id, count.index)}"
  }
  evaluation_periods        = "2"
  insufficient_data_actions = []
  metric_name               = "StatusCheckFailed_System"
  namespace                 = "AWS/EC2"
  period                    = "60"
  statistic                 = "Average"
  threshold                 = "0"
}
