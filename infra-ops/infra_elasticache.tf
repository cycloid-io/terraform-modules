resource "aws_elasticache_subnet_group" "cache-subnet" {
  name = "subnet-cache"
  description = "redis cache subnet"
  subnet_ids = ["${split(",", var.private_subnets_ids)}"]
}