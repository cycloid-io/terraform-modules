resource "aws_security_group" "monitoring" {
  name        = "monitoring"
  description = "Allow external dialog"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 5671
    to_port     = 5671
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port       = 4567
    to_port         = 4567
    protocol        = "tcp"
    cidr_blocks     = ["${var.vpc_cidr}",
                       "52.17.246.0/32"]
    security_groups = ["${aws_security_group.bastion.id}"]
  }

  tags {
    Name     = "monitoring"
    client   = "${var.customer}"
    env      = "infra"
    project  = "infra"
  }
}

resource "aws_eip" "monitoring" {
  instance = "${element(aws_instance.monitoring.*.id, count.index)}"
  vpc      = true
}

resource "aws_instance" "monitoring" {
  ami                  = "${lookup(var.debian_jessie_ami, var.aws_region)}"
  count = 1
  iam_instance_profile = "${var.iam_instance_profile}"
  instance_type        = "${var.monitoring_instance_type}"
  key_name             = "${var.keypair_name}"
  vpc_security_group_ids      = [
    "${aws_security_group.allow_bastion.id}",
    "${aws_security_group.monitoring.id}"
  ]
  subnet_id            = "${element(split(",", var.public_subnets_ids), count.index)}"

  tags {
    Name     = "${upper(var.customer)}-MONITORING${count.index}-${upper(lookup(var.short_region, var.aws_region))}-INFRA"
    client   = "${var.customer}"
    env      = "infra"
    project  = "infra"
    role     = "monitoring"
  }
}

##CLOUDWATCH ALARMS
resource "aws_cloudwatch_metric_alarm" "recover-monitoring" {
    alarm_name = "recover-monitoring"
    comparison_operator = "GreaterThanThreshold"
    evaluation_periods = "2"
    metric_name = "StatusCheckFailed_System"
    namespace = "AWS/EC2"
    period = "60"
    dimensions = {
        InstanceId = "${element(aws_instance.monitoring.*.id, count.index)}"
    }
    statistic = "Average"
    threshold = "0"
    alarm_description = "Recover the instance"
    insufficient_data_actions = []
    alarm_actions = ["arn:aws:automate:${var.aws_region}:ec2:recover"]
}
