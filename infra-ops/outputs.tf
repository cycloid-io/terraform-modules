# Outputs
output "bastion_private_ip" {
  value = "${join(",", aws_eip.bastion.*.private_ip)}"
}

output "bastion_public_ip" {
  value = "${join(",", aws_eip.bastion.*.public_ip)}"
}

output "bastion_sg" {
  value = "${aws_security_group.bastion.id}"
}

output "bastion_sg_allow" {
  value = "${aws_security_group.allow_bastion.id}"
}

output "metrics_private_ip" {
    value = "${join(",", aws_eip.metrics.*.private_ip)}"
}

output "metrics_public_ip" {
    value = "${join(",", aws_eip.metrics.*.public_ip)}"
}

output "monitoring_private_ip" {
    value = "${join(",", aws_eip.monitoring.*.private_ip)}"
}

output "monitoring_public_ip" {
    value = "${join(",", aws_eip.monitoring.*.public_ip)}"
}

output "iam_alerting_key" {
  value = "${aws_iam_access_key.alerting.id}"
}

output "iam_alerting_secret" {
  value = "${aws_iam_access_key.alerting.secret}"
}

output "cache_subnet" {
  value = "${aws_elasticache_subnet_group.cache-subnet.id}"
}

output "rds_subnet" {
  value = "${aws_db_subnet_group.rds-subnet.id}"
}

output "rds_parameters" {
  value = "${aws_db_parameter_group.rds-optimized.id}"
}