resource "aws_security_group" "bastion" {
  name        = "bastion"
  description = "Allow SSH traffic from the internet"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${split(",", var.bastion_allowed_networks)}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name     = "bastion"
    client   = "${var.customer}"
    project  = "infra"
  }
}

resource "aws_security_group" "allow_bastion" {
  name        = "allow_bastion"
  description = "Allow SSH traffic from the bastion"
  vpc_id      = "${var.vpc_id}"

  ingress     = {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${aws_security_group.bastion.id}"]
    self            = false
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name     = "allow-bastion"
    client   = "${var.customer}"
    project  = "infra"
  }
}

resource "aws_eip" "bastion" {
  instance = "${element(aws_instance.bastion.*.id, count.index)}"
  vpc      = true
}

resource "aws_instance" "bastion" {
  ami             = "${lookup(var.debian_jessie_ami, var.aws_region)}"
  instance_type   = "${var.bastion_instance_type}"
  key_name        = "${var.keypair_name}"
  vpc_security_group_ids = [
    "${aws_security_group.bastion.id}"
  ]
  iam_instance_profile = "${var.iam_instance_profile}"
  count = 1
  subnet_id = "${element(split(",", var.public_subnets_ids), count.index)}"

  tags {
    Name     = "${upper(var.customer)}-BASTION${count.index}-${upper(lookup(var.short_region, var.aws_region))}-INFRA"
    client   = "${var.customer}"
    env      = "infra"
    project  = "infra"
    role     = "bastion"
  }
}

##CLOUDWATCH ALARMS
resource "aws_cloudwatch_metric_alarm" "recover-bastion" {
  alarm_actions = ["arn:aws:automate:${var.aws_region}:ec2:recover"]
  alarm_description = "Recover the instance"
  alarm_name = "recover-bastion"
  comparison_operator = "GreaterThanThreshold"
  dimensions = {
    InstanceId = "${element(aws_instance.bastion.*.id, count.index)}"
  }
  evaluation_periods = "2"
  insufficient_data_actions = []
  metric_name = "StatusCheckFailed_System"
  namespace = "AWS/EC2"
  period = "60"
  statistic = "Average"
  threshold = "0"
}
