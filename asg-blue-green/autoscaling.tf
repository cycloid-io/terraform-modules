# Autoscaling
resource "aws_autoscaling_group" "autoscaling-group" {
    # Should have been working but https://github.com/hashicorp/terraform/issues/2359#issuecomment-161557763
    #lifecycle { create_before_destroy = true }

    name                 = "${var.tag_project}-${var.tag_role}-${var.tag_env} - ${var.blue_green_value}"
    launch_configuration = "${aws_launch_configuration.launch-configuration.name}"
    desired_capacity     = "${var.asg_desired_capacity}"
    min_size             = "${var.asg_min_size}"
    max_size             = "${var.asg_max_size}"
    min_elb_capacity     = "${var.asg_min_elb_capacity}"
    vpc_zone_identifier  = ["${split(",", var.asg_vpc_zone_identifier)}"]
    load_balancers       = ["${split(",", var.asg_load_balancers)}"]
    health_check_type    = "${var.asg_health_check_type}"
    health_check_grace_period = "${var.asg_health_check_grace_period}"
    tag {
      key = "Name"
      value = "${var.tag_name}"
      propagate_at_launch = true
    }

    tag {
      key = "client"
      value = "${var.tag_client}"
      propagate_at_launch = true
    }

    tag {
      key = "env"
      value = "${var.tag_env}"
      propagate_at_launch = true
    }

    tag {
      key = "project"
      value = "${var.tag_project}"
      propagate_at_launch = true
    }

    tag {
      key = "role"
      value = "${var.tag_role}"
      propagate_at_launch = true
    }

    tag {
      key = "asg"
      value = "true"
      propagate_at_launch = true
    }
}
