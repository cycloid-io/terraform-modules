# Userdata template
resource "template_file" "user-data" {
    template = "${file("${path.module}/user-data.tpl")}"
    vars {
      env = "${var.userdata_env}"
      customer = "${var.userdata_customer}"
      project = "${var.userdata_project}"
      role = "${var.userdata_role}"
    }
}

# Launch configuration
resource "aws_launch_configuration" "launch-configuration" {
    # Should have been working but https://github.com/hashicorp/terraform/issues/2359#issuecomment-161557763
    #lifecycle { create_before_destroy = true }

    image_id        = "${var.lc_ami}"
    instance_type   = "${var.lc_instance_type}"
    key_name        = "${var.lc_key_name}"
    security_groups = ["${split(",", var.lc_security_groups)}"]
    iam_instance_profile = "${var.lc_iam_instance_profile}"
    user_data = "${template_file.user-data.rendered}"
    root_block_device = {
      volume_type = "${var.lc_root_volume_type}"
      volume_size = "${var.lc_root_volume_size}"
    }

}