#!/bin/bash
echo "127.0.0.1 $(hostname)" >> /etc/hosts ;
sed -i "s/^- hosts:.*/- hosts: `curl -s http://169.254.169.254/latest/meta-data/local-ipv4`/" /home/admin/${customer}/${project}.yml ;
/usr/local/bin/ansible-playbook /home/admin/${customer}/${project}.yml --connection=local --tags "${role}" -e "env=${env}" --vault-password=/home/admin/${customer}/.vault-password >> /home/admin/bootstrap-provisionning.log 2>&1