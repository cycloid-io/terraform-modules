## Overview

AMI are built with Packer and Ansible

Every time a new AMI is built, we want to create a new Launch Configuration (LC) and Auto Scaling Group (ASG) running this AMI,
and bring the fresh EC2 instances into circulation within an existing Load Balancer (ELB).

Finally, once we've verified the new ASG instances are working well, we can delete the old LC and ASG, which will shut down
any instances running the older AMI.

## Terraform Workflow

Here are the steps to handle an AMI deployment:
(in this example, we're switching from Blue to Green)

 1. Update AMI ID for green module
 2. Enable green module
 3. Terraform `plan` then `apply`
 4. Verify new AMI deployment is working
 5. Disable blue module
 6. Terraform `plan` then `apply`

## Terraform Setup

Example how to call the module :

    #/* BLUE (Prefix with # to ENABLE, leave as /* to DISABLE)
    module "client-project-front-eu-we1_preprod-blue" {
        source                   = "bitbucket.org/cycloid-io/terraform-modules//asg-blue-green/"
        blue_green_value         = "blue"
        userdata_env             = "preprod"
        userdata_customer        = "MYCUSTOMER"
        userdata_project         = "MYPROJECT"
        userdata_role            = "front"
        lc_ami                   = "${var.project_MYPROJECT_front_preprod.ami}"
        lc_instance_type         = "${var.project_MYPROJECT_front_preprod.instance_type}"
        lc_key_name              = "${var.key_name}"
        lc_iam_instance_profile  = "${aws_iam_instance_profile.infra_profile.name}"
        lc_security_groups       = "${aws_security_group.allow_bastion.id},${aws_security_group.client-project-front-eu-we1_preprod.id}"
        lc_root_volume_type      = "${var.project_MYPROJECT_front_preprod.root_volume_type}"
        lc_root_volume_size      = "${var.project_MYPROJECT_front_preprod.root_volume_size}"
        asg_desired_capacity     = "${var.project_MYPROJECT_front_preprod.desired_capacity}"
        asg_min_size             = "${var.project_MYPROJECT_front_preprod.min_size}"
        asg_max_size             = "${var.project_MYPROJECT_front_preprod.max_size}"
        asg_min_elb_capacity     = "${var.project_MYPROJECT_front_preprod.min_elb_capacity}"
        asg_vpc_zone_identifier  = "${aws_subnet.public.0.id},${aws_subnet.public.1.id},${aws_subnet.public.2.id}"
        asg_load_balancers       = "${aws_elb.client-project-elb-front-eu-we1_preprod.id}"
        tag_name                 = "CLIENT-PROJECT-FRONT-EU-WE1-PREPROD"
        tag_client               = "${var.client}"
        tag_env                  = "preprod"
        tag_project              = "MYPROJECT"
        tag_role                 = "front"
    }
    /*
    */
    
    /* GREEN (Prefix with # to ENABLE, leave as /* to DISABLE)
    module "client-project-front-eu-we1_preprod-green" {
        source                   = "bitbucket.org/cycloid-io/terraform-modules//asg-blue-green/"
        blue_green_value         = "green"
        userdata_env             = "preprod"
        userdata_customer        = "MYCUSTOMER"
        userdata_project         = "MYPROJECT"
        userdata_role            = "front"
        lc_ami                   = "${var.project_MYPROJECT_front_preprod.ami}"
        lc_instance_type         = "${var.project_MYPROJECT_front_preprod.instance_type}"
        lc_key_name              = "${var.key_name}"
        lc_iam_instance_profile  = "${aws_iam_instance_profile.infra_profile.name}"
        lc_security_groups       = "${aws_security_group.allow_bastion.id},${aws_security_group.client-project-front-eu-we1_preprod.id}"
        lc_root_volume_type      = "${var.project_MYPROJECT_front_preprod.root_volume_type}"
        lc_root_volume_size      = "${var.project_MYPROJECT_front_preprod.root_volume_size}"
        asg_desired_capacity     = "${var.project_MYPROJECT_front_preprod.desired_capacity}"
        asg_min_size             = "${var.project_MYPROJECT_front_preprod.min_size}"
        asg_max_size             = "${var.project_MYPROJECT_front_preprod.max_size}"
        asg_min_elb_capacity     = "${var.project_MYPROJECT_front_preprod.min_elb_capacity}"
        asg_vpc_zone_identifier  = "${aws_subnet.public.0.id},${aws_subnet.public.1.id},${aws_subnet.public.2.id}"
        asg_load_balancers       = "${aws_elb.client-project-elb-front-eu-we1_preprod.id}"
        tag_name                 = "CLIENT-PROJECT-FRONT-EU-WE1-PREPROD"
        tag_client               = "${var.client}"
        tag_env                  = "preprod"
        tag_project              = "MYPROJECT"
        tag_role                 = "front"
    }
    /*
    */
    
