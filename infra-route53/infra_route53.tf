# Create route53 zones
## Public zone
resource "aws_route53_zone" "public" {
   name = "${var.public_zone_name}"

   tags {
       client = "${var.customer}"
   }
}

## Private zone
resource "aws_route53_zone" "private" {
    name = "${var.private_zone_name}"
    vpc_id = "${var.vpc_id}"
    tags {
      client = "${var.customer}"
    }
}
