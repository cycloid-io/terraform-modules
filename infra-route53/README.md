## Overview

This terraform module is used at cycloid in order to the DNS for the infrastructure.
It will create a private and a public zone

## Terraform Setup

Example how to call the module :

    module "infra-route53" {
      source = "bitbucket.org/cycloid-io/terraform-modules//infra-route53/"
      
      customer             = "cycloid"
      public_zone_name     = "infra.cycloid.io"
      private_zone_name    = "infra.cycloid.internal"
      vpc_id               = "
    }
    
## Outputs


* `public_zone_id`

* `private_zone_id`
