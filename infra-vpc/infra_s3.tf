# Create backup S3 repo
resource "aws_s3_bucket" "backup" {
  bucket = "${var.customer}-backup"
  acl = "private"
  tags {
    Name    = "${var.customer}-backup"
    client  = "${var.customer}"
    project = "backup"
  }
}

# Create a bucket for terraform remote state
resource "aws_s3_bucket" "terraform-remote-state" {
    bucket = "${var.customer}-terraform-remote-state"
    acl = "private"

    versioning {
        enabled = true
    }

    tags {
        Name    = "terraform-remote-state"
        client  = "${var.customer}"
        project = "infra"
    }
}
