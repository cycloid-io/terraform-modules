# Create IAM users
resource "aws_iam_user" "infra" {
  name = "infra"
  path = "/cycloid/"
}


resource "aws_iam_user" "ses-smtp-user" {
  name = "ses-smtp-user-${var.customer}"
  path = "/cycloid/"
}

resource "aws_iam_access_key" "infra" {
  user = "${aws_iam_user.infra.name}"
}


resource "aws_iam_access_key" "ses-smtp-user" {
  user = "${aws_iam_user.ses-smtp-user.name}"
}

resource "aws_iam_user_policy" "dns_push_record" {
  name = "r53-push-record"
  user = "${aws_iam_user.infra.name}"
  policy = "${file("${path.module}/policies/iam-infra-dns-push-record.json")}"
}

resource "aws_iam_user_policy" "backup" {
  name = "s3-backup"
  user = "${aws_iam_user.infra.name}"
  policy = "${file("${path.module}/policies/iam-infra-s3-backup.json")}"
}

resource "aws_iam_user_policy" "ec2_snapshots" {
  name = "ec2-snapshots"
  user = "${aws_iam_user.infra.name}"
  policy = "${file("${path.module}/policies/iam-infra-ec2-snapshot.json")}"
}

resource "aws_iam_user_policy" "ec2_startstop" {
  name = "ec2-startstop"
  user = "${aws_iam_user.infra.name}"
  policy = "${file("${path.module}/policies/iam-infra-ec2-startstop.json")}"
}

resource  "aws_iam_user_policy" "deploy" {
  name = "deploy"
  user = "${aws_iam_user.infra.name}"
  policy = "${file("${path.module}/policies/iam-infra-deploy.json")}"
}


resource "aws_iam_user_policy" "SesSendingAccess" {
  name = "ses-sending-access"
  user = "${aws_iam_user.ses-smtp-user.name}"
  policy = "${file("${path.module}/policies/iam-infra-ses-sending-access.json")}"
}

resource "aws_iam_policy_attachment" "infra-readonly" {
  name = "infra-readonly"
  users = ["${aws_iam_user.infra.name}"]
  roles = ["${aws_iam_role.infra.name}"]
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

# Create IAM Role
resource "aws_iam_role" "infra" {
  name = "infra"
  assume_role_policy = "${file("${path.module}/policies/iam-role-infra.json")}"
}

resource "aws_iam_role_policy" "dns_push_record" {
  name = "r53-push-record"
  role = "${aws_iam_role.infra.id}"
  policy = "${file("${path.module}/policies/iam-infra-dns-push-record.json")}"
}

resource "aws_iam_role_policy" "backup" {
  name = "s3-backup"
  role = "${aws_iam_role.infra.id}"
  policy = "${file("${path.module}/policies/iam-infra-s3-backup.json")}"
}

resource "aws_iam_instance_profile" "infra_profile" {
  name = "infra_profile"
  roles = ["${aws_iam_role.infra.name}"]
}

