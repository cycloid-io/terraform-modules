# Outputs

output "aws_vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "aws_internet_gateway_id" {
  value = "${aws_internet_gateway.main.id}"
}
output "iam_instance_profile" {
  value = "${aws_iam_instance_profile.infra_profile.name}"
}

output "public_subnets_ids" {
  value = "${join(",", aws_subnet.public.*.id)}"
}

output "private_subnets_ids" {
  value = "${join(",", aws_subnet.private.*.id)}"
}

output "private_route_table_ids" {
  value = "${join(",", aws_route_table.private.*.id)}"
}

output "nat_eips" {
  value = "${join(",", aws_eip.nat_gateway.*.public_ip)}"
}

output "iam_infra_key" {
  value = "${aws_iam_access_key.infra.id}"
}

output "iam_infra_secret" {
  value = "${aws_iam_access_key.infra.secret}"
}

output "iam_ses-smtp-user_key" {
    value = "${aws_iam_access_key.ses-smtp-user.id}"
}

output "iam_ses-smtp-user_secret" {
    value = "${aws_iam_access_key.ses-smtp-user.ses_smtp_password}"
}

output "keypair_name" {
    value = "${aws_key_pair.cycloid.id}"
}