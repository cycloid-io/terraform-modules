variable "aws_region" {
  default = "eu-west-1"
}
variable "azs" {
}
variable "cidr_private_blocks" {
}
variable "cidr_public_blocks" {
}
variable "customer" {
}
variable "cycloid_keypair_public" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDv1GWqrox06+fiaSseYY6EK8dFXtmsS0gGX3xMXAdiAe16Bx4fFPvt9Q/tgCx4t4BjLD40MQVjFtVoDPEOg+LvndBF8hmAfdgxaqFwjTbs9LT3p1TIPePb5xiB9iWODlghfEujqgsXYAK7WjODkDcBjiNGhooco+4BvW3CIiEhy5R1ArJDoGdSiWVdlUDxKSsQxMgco8VHj6u/JyWQddSKpryywJ7JfwnHvb3zRF4pboYJRbZnYfQEI99D53M8rJOEpMTER7ZErrfdijnuYR+Z61W0CE/TkOHmnL5b6A7nDvmM5iCHmnsLeDrIhtFiHlubfHB7brF4VSGU/TqOb3s+2h3F4c6yjBBBqXMH/VEdgcFmtVu4BnRlvBV8w/Z0kYU93wQB9FfH7oWdWnsLFimucY44IpGKRnKaYiu9w2ezUoN9REYRZ8mUxgMVzd1t2iRuX6HjE3hPFC3Sku8pW22sHrHmsNkQ6mOOr+u+jUTEDu5yS23gyN3Mv/m93mALHNSpCcOMI9t3isTdwoFdtX2vpcmyYgJ6yB7aAzQXWkI9BcxXEbLEUrgbv1dRE6WDz0+uznE1UJ2TsRt+OBL9M7DPdRKrO1aNST3lKgRmqxlcBmC/k+gr9uPJD/Jea4SpybIHwWAsXhs5/xgTZic3Y5tKI2nni4OWfz8xy52UaTsJwQ== cycloid-admin"
}
variable "debian_jessie_ami" {
  default = {
    ap-northeast-1 = "ami-d7d4c5b9"
    ap-northeast-2 = "ami-9a03caf4"
    ap-southeast-1 = "ami-73974210"
    ap-southeast-2 = "ami-09daf96a"
    eu-central-1   = "ami-ccc021a3"
    eu-west-1      = "ami-e079f893"
    sa-east-1      = "ami-d3ae21bf"
    us-east-1      = "ami-c8bda8a2"
    us-west-1      = "ami-45374b25"
    us-west-2      = "ami-98e114f8"
  }
}

variable "short_region" {
  default = {
    ap-northeast-1 = "ap-no1"
    ap-northeast-2 = "ap-no2"
    ap-southeast-1 = "ap-so1"
    ap-southeast-2 = "ap-so2"
    eu-central-1   = "eu-ce1"
    eu-west-1      = "eu-we1"
    sa-east-1      = "sa-ea1"
    us-east-1      = "us-ea1"
    us-west-1      = "us-we1"
    us-west-2      = "us-we2"
  }
}

variable "keypair_name" {
  default = "cycloid"
}
variable "nat_gateway_count" {
}
variable "private_zone_name" {
}
variable "vpc_cidr" {
}
