resource "aws_eip" "nat_gateway" {
  vpc = true
  count = "${var.nat_gateway_count}"
}

resource "aws_nat_gateway" "gateway" {
  allocation_id = "${element(aws_eip.nat_gateway.*.id, count.index)}"
  count = "${var.nat_gateway_count}"
  subnet_id = "${element(aws_subnet.public.*.id, count.index)}"
}
