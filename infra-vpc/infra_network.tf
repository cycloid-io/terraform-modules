resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags {
    Name   = "${var.customer}"
    client = "${var.customer}"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_subnet" "public" {
  depends_on = ["aws_internet_gateway.main"]
  vpc_id = "${aws_vpc.main.id}"
  cidr_block = "${element(split(",", var.cidr_public_blocks), count.index)}"
  availability_zone = "${element(split(",", var.azs), count.index)}"
  map_public_ip_on_launch = true
  count = "${length(split(",", var.cidr_public_blocks))}"
  tags {
    Name     = "public-subnet-${element(split(",", var.azs), count.index)}"
	client   = "${var.customer}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
  count = "${length(split(",", var.cidr_public_blocks))}"
}

resource "aws_subnet" "private" {
  vpc_id = "${aws_vpc.main.id}"
  cidr_block = "${element(split(",", var.cidr_private_blocks), count.index)}"
  availability_zone = "${element(split(",", var.azs), count.index)}"
  count = "${length(split(",", var.cidr_private_blocks))}"
  tags {
    Name     = "private-subnet-${element(split(",", var.azs), count.index)}"
	client   = "${var.customer}"
  }
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.main.id}"
  count  = "${length(split(",", var.cidr_private_blocks))}"
}

resource "aws_route_table_association" "private" {
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
  count          = "${length(split(",", var.cidr_private_blocks))}"
}

resource "aws_route" "nat_gateway" {
  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.gateway.*.id, count.index)}"
  count                  = "${length(split(",", var.cidr_private_blocks))}"

  depends_on             = ["aws_route_table.private"]
}
