# Create DHCP options set
resource "aws_vpc_dhcp_options" "dhcp-internal" {
  domain_name = "${var.private_zone_name}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags {
    Name = "kubernetes-dhcp-${var.customer}-internal"
    client = "${var.customer}"
  }
}

# Association between DHCP and VPC
resource "aws_vpc_dhcp_options_association" "dns-internal" {
  vpc_id = "${aws_vpc.main.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp-internal.id}"
}
