## Overview

This terraform module is used at cycloid in order to create a VPC, the network, and DNS stuff.

## Terraform Setup

Example how to call the module :

    module "infra-vpc" {
      source = "bitbucket.org/cycloid-io/terraform-modules//infra-vpc/"
      
      customer             = "cycloid"
      vpc_cidr             = "10.42.0.0/16"
      cidr_public_blocks   = "10.42.0.0/24,10.42.2.0/24,10.42.4.0/24"
      cidr_private_blocks  = "10.42.1.0/24,10.42.3.0/24,10.42.5.0/24"
      azs                  = "eu-west-1a,eu-west-1b,eu-west-1c"
      nat_gateway_count    = 1
      private_zone_name    = "infra.cycloid.internal"
    }
    
## Outputs

* `aws_vpc_id`

* `aws_internet_gateway_id`

* `public_subnets_ids`

* `private_subnets_ids`

* `private_route_table_ids`

* `nat_eips`
