resource "aws_key_pair" "cycloid" {
  key_name   = "${var.keypair_name}"
  public_key = "${var.cycloid_keypair_public}"
}